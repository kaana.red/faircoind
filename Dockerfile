# Pull base image.
FROM ubuntu:18.04@sha256:5f4bdc3467537cbbe563e80db2c3ec95d548a9145d64453b06939c4592d67b6d

ENV LC_ALL=C.UTF-8 LANG=C.UTF-8

#####################################################################################root##
### install base libraries and dependencies for faircoin daemon ###########################
RUN apt-get update -q && \
    apt-get install -qy \
        git \
        make \
        autotools-dev \
        autoconf \
        libtool \
        libssl-dev \
        libboost-all-dev \
        libevent-dev \
        libdb++-dev \
        libdb-dev

#####################################################################################root##
### install nano commandline editor [optional] ( to edit faircoin.conf later if necessary )
RUN apt-get install -qy nano

#####################################################################################root##
### install python packages
RUN apt-get install -qy python3-pip
RUN python3 -m pip install \
        python-bitcoinrpc

#####################################################################################root##
### system cleanup ########################################################################
RUN rm -rf /var/lib/apt/lists/* && \
    apt-get autoremove -y && \
    apt-get clean

#####################################################################################root##
### create and run user account to image ##################################################
ARG RUNNER_GID
ARG RUNNER_UID
RUN groupadd -g $RUNNER_GID faircoin
RUN useradd --create-home --shell /bin/bash faircoin --uid $RUNNER_UID --gid $RUNNER_GID

USER faircoin
#################################################################################faircoin##
### download and build faircoin daemon and cli ############################################
WORKDIR /home/faircoin
ARG source_repository
RUN git clone $source_repository /home/faircoin/faircoin

WORKDIR /home/faircoin/faircoin
RUN ./autogen.sh && \
    ./configure --disable-tests --disable-bench --with-incompatible-bdb --disable-wallet && \
    make

#################################################################################faircoin##
### initialize blockchain with txindex=1 ##################################################
WORKDIR /home/faircoin/faircoin/src
RUN mv ./faircoind /home/faircoin/faircoind
RUN mv ./faircoin-cli /home/faircoin/faircoin-cli

USER root
###########################################################################################
### install dependencies for explorer.kaana.red ###################################root##
RUN python3 -m pip install \
        simplejson \
        requests \
        interruptingcow \
        PyMySQL

RUN mkdir -p /home/faircoin/.faircoin2
RUN chown -R faircoin:faircoin /home/faircoin/.faircoin2

RUN mkdir -p /home/faircoin/explorer.kaana.red
RUN chown -R faircoin:faircoin /home/faircoin/explorer.kaana.red

USER faircoin
WORKDIR /home/faircoin
ENTRYPOINT ["./faircoind"]
