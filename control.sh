#!/bin/bash
export RUNNER_UID=`id -u`
export RUNNER_GID=`id -g`
export `cat ./env/*`

case $1 in
  "build")
      mkdir -p ~/.faircoin2
      chmod +x ./script/blocknotify.sh
      docker-compose stop
      docker-compose down
      docker-compose build
    ;;
  "start")
      chmod +x ./script/blocknotify.sh
      docker-compose stop
      docker-compose down
      source ${FAIRCOIN_CONF}
      if [ ! -f "~/.faircoin2/faircoin.conf" ]; then echo -e "rpcconnect=${rpcconnect}\nrpcport=${rpcport}\nrpcuser=${rpcuser}\nrpcpassword=${rpcpassword}\ntxindex=1" > ~/.faircoin2/faircoin.conf; fi
      docker-compose up -d
    ;;
  "stop")
      docker-compose stop
    ;;
  "uninstall")
      docker-compose stop
      docker-compose down
    ;;
  "remove")
      docker-compose stop
      docker-compose down
#      rm -R ~/.faircoin2  #### dont use this command if you have some wallet.dat in your ~/.faircoin2 folder !!! In this case remove the block databases manually.
    ;;
esac
